@startuml
class "VertragsEreignis" as ContractEvent {
    Zeitpunkt
} 

ContractEvent <|-- "Objekt hinzugefügt"
ContractEvent <|-- "Objekt entfernt"
ContractEvent <|-- "Verwalteränderung"
ContractEvent <|-- "etc"

class "ObjektEreignis" as Event {
    Zeitpunkt
}
Event <|-- Schätzung
Event <|-- Nachtrag
Event <|-- Abbruch
Event <|-- Entlassung
@enduml