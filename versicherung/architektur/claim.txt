@startuml

state "Schaden erfassen" as create
state "Teilablauf währen" as chooseNext
state "Triagieren" as triage
state "Auszahlung" as payout
state "in Arbeit" as inProgress
state "Augenschein" as onSiteInspection
state "erste Freigabestufe" as clearance1
state "zweite Freigabestufe" as clearance2
state "bereit zum Versand" as readyForDelivery

state initial_fork_state <<fork>>
state clearance_fork_state <<fork>>

[*] --> create
create --> initial_fork_state
initial_fork_state --> chooseNext : Assistent
initial_fork_state --> triage : TopaX
chooseNext --> onSiteInspection
chooseNext --> payout : direkt zur Auszahlung bringen
payout --> clearance1 
onSiteInspection --> inProgress : Augenschein erledigen
inProgress --> onSiteInspection : neuen Augenschein planen
inProgress --> clearance1 : Kundeninformation erstellen
clearance1 --> clearance_fork_state
clearance_fork_state --> clearance2 : Schadensumme > X
clearance_fork_state --> readyForDelivery
clearance2 --> readyForDelivery : Dokumente und Auszahlung freigeben
readyForDelivery --> inProgress

note left of create
Die Übergänge von einem Status zum anderen sind sogenannte Aktivitäten
Dies ist nur eine Beispielhafte Darstellung eines Workflows. 
Die konkreten Workflows / States / Activiteies sind kundenspezifisch programmiert
end note
  
}
@enduml